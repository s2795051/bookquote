package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    Map<String, Double> isbnList = new HashMap<>();

    Quoter() {
        isbnList.put("1", 10.0);
        isbnList.put("2", 45.0);
        isbnList.put("3", 20.0);
        isbnList.put("4", 35.0);
        isbnList.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        if (isbnList.keySet().contains(isbn))
            return isbnList.get(isbn);
        else
            return 0.0;
    }
}
